<section id="block_hero">

    <div class="hero__photo">
        <img class="image" src="<?= get_template_directory_uri() . "/images/fifa.png"; ?>" alt="Lorem Ipsum logo" />
    </div>

    <div class="container">

        <div class="row">
            <div class=" col-12 col-md-6 hero__content">

                <h1 class="title" data-aos="fade-up" data-aos-duration="1500"><?php the_field('header') ?></h1>

                <img class="lines" data-aos="fade-up" data-aos-duration="1500"
                    src="<?= get_template_directory_uri() . "/images/lines.png"; ?>" alt="dot dot" />

                <div data-aos="fade-up" data-aos-duration="1500">
                    <p class="text"><?php the_field('text') ?></p>
                </div>

                <button data-aos="fade-up" data-aos-duration="1500"><?php the_field('buttonText') ?></button>

                <img class="circles" data-aos="fade-up" data-aos-duration="1500"
                    src="<?= get_template_directory_uri() . "/images/circles.png"; ?>" alt="dot dot" />
                    
                <img class="arrow" data-aos="fade-up" data-aos-duration="1500"
                    src="<?= get_template_directory_uri() . "/images/arrow.svg"; ?>" alt="arrow down" />

            </div>
            <div class="col-6">
            </div>
        </div>
    </div>

</section>