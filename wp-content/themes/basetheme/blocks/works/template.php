<section id="block_works">

    <div class="container">

        <div class="row">
            <div class="col-12" data-aos="fade-up" data-aos-duration="1500">
                <h1><?php the_field('header') ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 center" data-aos="fade-up" data-aos-duration="1500">
                <img class="image" src="<?= get_template_directory_uri() . "/images/dot.png"; ?>" alt="dot dot" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 offer">
                <?php
                    if (have_rows('card')) :

                        while (have_rows('card')) : the_row();
                            $icon = get_sub_field('icon');
                            $photo = get_sub_field('photo');
                            $title = get_sub_field('title');
                            $text = get_sub_field('text');
                            $delay += 200;
                ?>

                <?php
                    echo '<div class="offer__simple" data-aos="fade-up" data-aos-delay="'.$delay.'" data-aos-duration="600">'

                 ?>

                <div class="photo">
                    <?php
                        echo '<img class="icon" src="' . $photo . '" alt="' . $title . '"/>';
                    ?>
                </div>

                <div class="offer__simple--top">

                    <div>
                        <?php
                            echo '<h3 class="title">' . $title . '</h3>';
                        ?>
                    </div>
                    <div>
                        <?php
                                echo '<img class="icon" src="' . $icon . '" alt="' . $title . '"/>';
                            ?>
                    </div>

                </div>
                <div class="offer__simple--bottom">
                    <?php
                        echo '<p class="text">' . $text . '</p>';
                    ?>
                </div>

                <?php
                    echo '</div>'
                ?>

                <?php
                endwhile;
                endif;
                ?>

            </div>
        </div>

    </div>
</section>