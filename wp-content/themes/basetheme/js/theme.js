$(document).ready(function () {

    initCookieInfo();
    aosStart();

});

/* ------------------------------------------------------------------------------------------ */

function initCookieInfo() {

    if (ajax_options.cookies_enabled == "true") {
        if (checkCookie(window.cookieName) != window.cookieValue) {

            var data = {
                'action': 'test'
            };

            jQuery.post(ajax_options.admin_ajax_url, data, function (response) {

                createDiv(response);
            });

        }
    }
}

var dropCookie = true;
var cookieDuration = 14;
var cookieName = 'complianceCookie';
var cookieValue = 'on';

function createDiv(content) {

    var content_array = JSON.parse(content);

    var bodytag = document.getElementsByTagName('body')[0];
    var div = document.createElement('div');
    div.setAttribute('id', 'cookie-law');
    div.innerHTML = '<div>' + content_array.cookies_text + '</div><div><a class="close-cookie-banner" href="javascript:void(0);" onclick="removeMe();"><span>' + content_array.cookies_button_label + '</span></a></div></div>';

    bodytag.appendChild(div);
    document.getElementsByTagName('body')[0].className += ' cookiebanner';

}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else {
        var expires = "";
    }
    if (window.dropCookie) {
        document.cookie = name + "=" + value + expires + "; path=/";
    }
}

function checkCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function removeMe() {

    createCookie(window.cookieName, window.cookieValue, window.cookieDuration); // Create the cookie

    var element = document.getElementById('cookie-law');
    element.parentNode.removeChild(element);
}

function aosStart() {
    AOS.init();
}