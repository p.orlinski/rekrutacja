<footer id="colophon" class="site-footer">

    <div class="container">

        <div class="row">
            <div class="col-12 col-md-3 left">
                Copyrights &copy; 2019 by Kerris Group.
                All rights reserved.
            </div>
            <div class="col-12 col-md-9 right">
                <img src="<?= get_template_directory_uri() . "/images/circles.png"; ?>" alt="">
            </div>

        </div>
    </div>

</footer>
</div>


<?php wp_footer(); ?>

<?php

    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:82") {
        echo '<script src="//localhost:35729/livereload.js"></script>';
    }

?>

</body>

</html>