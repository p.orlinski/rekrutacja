<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Kerris - projekt rekrutacyjny na stanowisko Web Developer">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/style_blocks.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/js/aos/aos.css">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600&display=swap" rel="stylesheet">

    <link rel="icon" href="<?= get_template_directory_uri(); ?>/images/favico.png" />

    <script src="<?= get_template_directory_uri(); ?>/js/jquery-3.4.1.min.js" type="text/javascript"></script>
    <script src="<?= get_template_directory_uri(); ?>/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="<?= get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <script src="<?= get_template_directory_uri(); ?>/js/slick.min.js"></script>
    <script src="<?= get_template_directory_uri(); ?>/js/aos/aos.js"></script>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="page" class="site">
        <a class="skip-link screen-reader-text"
            href="#content"><?php esc_html_e('Skip to content', 'basetheme'); ?></a>

        <header id="masthead" class="site-header">

            <div class="container">

                <div class="row">
                    <div class="col-6">

                        <div class="site-branding">

                            <div class="logo">
                                <a href="<?= site_url(); ?>"><img
                                        src="<?= get_template_directory_uri() . "/images/logo.png"; ?>"
                                        alt="Lorem Ipsum logo" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>

        <div id="content" class="site-content">